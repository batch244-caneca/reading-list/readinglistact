/*Review and Practice*/

let number = 10;
let remainder = (number % 2);

if (remainder == 0) {
	console.log(number + " is even.");
} else {
	console.log(number + " is odd.");
}

function favCharacters() {
	alert("Who are your top 3 movie characters?");
	let firstCharacter = prompt("Enter your top 1 character:");
	let secondCharacter = prompt("Enter your top 2 character:");
	let thirdCharacter = prompt("Enter your top 3 character:");
	alert("Thank you for your input!");

	console.log("Your Top 3 Favorite Movie Characters are:");
	console.log(firstCharacter);
	console.log(secondCharacter);
	console.log(thirdCharacter);
}
favCharacters();

function yearlyIncome() {
	let salaryPerDay = 1000;
	let workDays = 260;

	console.log(salaryPerDay * workDays);
}

yearlyIncome();


/*Activity A*/

function computeDogAge(humanYear, dogYear = 7) {
	console.log("Your doggie is " + humanYear * dogYear + " years old in dog years!");
}

computeDogAge(3);
computeDogAge(5);
computeDogAge(8);

/*Activity B*/

function grades(math, english, science) {
	if (math > 71, english > 71, science > 71) {
		return (math + english + science) / 3;
	}
}

console.log("Your average is " + grades(75, 75, 75) + ".");